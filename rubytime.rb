#! /usr/local/bin/ruby


require 'net/http'
require 'uri'
require 'benchmark'


def ping(host)
begin
        url=URI.parse(host)
        response=Net::HTTP.get(url)
        if response==""
                #puts "Response was empty"
                print "E"
                return false
        else
                #puts "Data returned"
                print "."
                return true
        end
        rescue Errno::ECONNREFUSED
                #puts "Error - Connection refused"
                print "!"
                return false
        end
end


puts "How to read this:"
puts " "
puts "P   . is successful data returned"
puts "    E is an empty page returned"
puts "    ! is an Error - Connection Refused"
puts " "
puts "Now is the current time"
puts " "
puts "Response Benchmark is the time it take do an HTTP.get(url)"
puts " "
print "P Now                      Response benchmark\n"


stop_time = Time.now + 5


while Time.now < stop_time do
        Time.now.strftime("%H:%M:%S")
        time = Benchmark.measure do
                #ping "http://www.google.com"
                ping "https://gitlab.com"
        end


        print " " + Time.now.strftime("%H:%M:%S") + " - Benchmark : #{time} ".chop
end
